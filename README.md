# README #

My version of the grid alignment that Code School made. 

### Features ###

* Includes column direction
* Includes wrapping for containers with four or more boxes
* Includes box-sizing 